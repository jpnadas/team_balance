import json
import itertools
import numpy as np


class Player:
    def __init__(self, **entries):
        self.__dict__.update(entries)

class Team:
    def __init__(self, players):
        self.players = players
        self.rating = None
        self.std = None

    def calc_metrics(self):
        ratings = []
        for p in self.players:
            ratings.append(p.rating)

        self.rating = np.sum(ratings)
        self.std = np.std(ratings)
        
class Matchup:
    def __init__(self, team_1, player_list):
        self.team_1 = team_1
        self.team_2 = self.obtain_team_2(player_list)
        self.diff = 1e3
        self.std_diff = 1e3
        self.check_balance()

    def obtain_team_2(self, player_list):

        # temp list of players for team 2
        players = []

        for p in player_list:
            if p not in self.team_1.players:
                players.append(p)

        return Team(players)

    def check_balance(self):
        self.team_1.calc_metrics()
        self.team_2.calc_metrics()
        self.diff = np.abs(self.team_1.rating - self.team_2.rating)
        self.std_diff = np.abs(self.team_1.std - self.team_2.std)

    def print_matchup(self):
        print('team 1')
        for p in self.team_1.players:
            print(p.name)
        print('team 2')
        for p in self.team_2.players:
            print(p.name)


def main():
    try:
        with open('./players.json', 'r') as json_file:
            players = json.load(json_file)
    except IOError:
        print('In order to run this script you must have a file named players.json with players and ratings')
        return

    player_list = []
    for p in players:
        player_list.append(Player(**p))

    n = len(player_list)
    comb = itertools.combinations(player_list, int(n/2))

    matchups = []

    n_matchups = np.math.factorial(n) / np.math.factorial(n / 2) / np.math.factorial(n / 2) / 2

    for i, c in enumerate(comb):
        if i == n_matchups:
            break
        matchups.append(Matchup(Team(c), player_list))

    smallest = min(matchups, key=lambda x: (x.diff, x.std_diff))
    possible = [v for v in matchups if v.diff == smallest.diff and v.std_diff == smallest.std_diff]
    if len(possible) == 0:
        possible = [v for v in matchups if v.diff == smallest.diff]
    
    choice = input(f'Choose a number between 1 and {len(possible)}: ')
    possible[int(choice) - 1].print_matchup()

main()

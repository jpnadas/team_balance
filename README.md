# Introduction

This is a simple script to calculate balanced teams based on scores. It takes into account user provided scores to generate teams with similar strength by comparing the teams aggregated score and standard deviation.

# Dependencies
* Python 3

## Module dependencies
* numpy
* json

# Installation
`git clone https://gitlab.com/jpnadas/team_balance.git`

# How to run
Populate a file named `players.json` with a list of players and their ratings, as in:

```
[
  {
    "name": "Player 1",
    "rating": 5
  },
  {
    "name": "Player 2",
    "rating": 2
  },
  ...
  {
    "name": "Player 10",
    "rating": 3
  }
]
```

then run the script `main.py` (`python3 main.py`).
Note that, the number of players on the file must be even.
